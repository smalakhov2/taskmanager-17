package ru.malakhov.tm.repository;

import ru.malakhov.tm.api.repository.IUserRepository;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public User add(final User user) {
        users.add(user);
        return user;
    }

    @Override
    public User findById(final String id) {
        for (final User user : users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User findByLogin(final String login) {
        for (final User user : users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User removeUser(final User user) {
        users.remove(user);
        return user;
    }

    @Override
    public User removeById(final String id) {
        final User user = findById(id);
        if (user == null) return null;
        return removeUser(user);
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return removeUser(user);
    }

    @Override
    public String[] profile(final String id) {
        String[] profileInfo = {"First name: " + findById(id).getFirstName(),
                                "Last name: " + findById(id).getLastName(),
                                "Middle name: " + findById(id).getMiddleName(),
                                "Login: " + findById(id).getLogin(),
                                "E-mail: " + findById(id).getEmail(),
                                "Account type: " + findById(id).getRole().getDisplayName()};
        return profileInfo;
    }

    @Override
    public User changeEmail(final String id, final String email) {
        final User user = findById(id);
        user.setEmail(email);
        return user;
    }

    @Override
    public User changePassword(final String id, final String password) {
        final User user = findById(id);
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User changeLogin(final String id, final String login) {
        final User user = findById(id);
        user.setLogin(login);
        return user;
    }

    @Override
    public User changeFirstName(final String id, final String name) {
        final User user = findById(id);
        user.setFirstName(name);
        return user;
    }

    @Override
    public User changeMiddleName(final String id, final String name) {
        final User user = findById(id);
        user.setMiddleName(name);
        return user;
    }

    @Override
    public User changeLastName(final String id, final String name) {
        final User user = findById(id);
        user.setLastName(name);
        return user;
    }

    @Override
    public void clear() {
        users.removeAll(users);
    }

    @Override
    public User merge(final User user) {
        if (user == null) return null;
        users.add(user);
        return user;
    }

    @Override
    public void merge(final User... users) {
        if (users == null) return;
        for (final User user: users) merge(user);
    }

    @Override
    public void merge(final Collection<User> users) {
        if (users == null) return;
        for (final User user: users) merge(user);
    }

    @Override
    public void load(final User... users) {
        if (users == null) return;
        clear();
        merge(users);
    }

    @Override
    public void load(final Collection<User> users) {
        if (users == null) return;
        clear();
        merge(users);
    }

    @Override
    public List<User> getUserList() {
        return new ArrayList<>(users);
    }

}