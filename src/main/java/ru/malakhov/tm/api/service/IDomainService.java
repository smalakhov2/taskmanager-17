package ru.malakhov.tm.api.service;

import ru.malakhov.tm.dto.Domain;

public interface IDomainService {

    void load(Domain domain);

    void export(Domain domain);

}
