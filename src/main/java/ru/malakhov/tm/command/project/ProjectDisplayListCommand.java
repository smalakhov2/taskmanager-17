package ru.malakhov.tm.command.project;

import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.TerminalConst;
import ru.malakhov.tm.entity.Project;

import java.util.List;

public final class ProjectDisplayListCommand extends AbstractCommand {

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.PROJECT_LIST;
    }

    @Override
    public String description() {
        return "Display task projects.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[DISPlAY PROJECTS]");
        final List<Project> projects = serviceLocator.getProjectService().findAll(userId);
        for (Project project : projects) System.out.println(project);
        System.out.println("[OK]");
    }

}