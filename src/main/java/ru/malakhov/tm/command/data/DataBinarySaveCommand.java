package ru.malakhov.tm.command.data;

import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.dto.Domain;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;

import static ru.malakhov.tm.constant.PathConst.BINARY_PATH;

public class DataBinarySaveCommand extends AbstractCommand {
    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return "binary-save";
    }

    @Override
    public String description() {
        return "Save binary data.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BINARY SAVE]");
        final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);
        final File file = new File(BINARY_PATH);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        fileOutputStream.close();
        System.out.println("[OK]");
    }

}
