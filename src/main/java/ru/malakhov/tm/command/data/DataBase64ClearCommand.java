package ru.malakhov.tm.command.data;

import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.PathConst;

import java.io.File;
import java.nio.file.Files;

import static ru.malakhov.tm.constant.PathConst.BASE64_PATH;

public class DataBase64ClearCommand extends AbstractCommand {

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return "db64-clear";
    }

    @Override
    public String description() {
        return "Clear base64 data.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 CLEAR]");
        final File file = new File(BASE64_PATH);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");

    }

}