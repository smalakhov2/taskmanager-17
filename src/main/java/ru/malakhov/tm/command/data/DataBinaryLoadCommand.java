package ru.malakhov.tm.command.data;

import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.dto.Domain;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

import static ru.malakhov.tm.constant.PathConst.BINARY_PATH;

public class DataBinaryLoadCommand extends AbstractCommand {
    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return "binary-load";
    }

    @Override
    public String description() {
        return "Load binary data.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BINARY LOAD]");
        final FileInputStream fileInputStream = new FileInputStream(BINARY_PATH);
        final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        final Domain domain = (Domain) objectInputStream.readObject();
        serviceLocator.getProjectService().load(domain.getProjects());
        serviceLocator.getTaskService().load(domain.getTasks());
        serviceLocator.getUserService().load(domain.getUsers());
        objectInputStream.close();
        fileInputStream.close();
        System.out.println("[OK]");

    }

}
