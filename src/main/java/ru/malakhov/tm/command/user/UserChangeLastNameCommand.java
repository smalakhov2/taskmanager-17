package ru.malakhov.tm.command.user;

import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.TerminalConst;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.util.TerminalUtil;

public final class UserChangeLastNameCommand extends AbstractCommand {

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.CHANGE_LAST_NAME;
    }

    @Override
    public String description() {
        return "Change last name.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE LAST NAME]");
        System.out.println("ENTER NEW LAST NAME:");
        final String name = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().changeLastName(userId, name);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}